import './App.css'

import {useState,useEffect} from 'react'

import Home from 'pages/home'
import NavBar from 'components/AppNavBar'
import Register from 'pages/register'
import Login from 'pages/login'
import AddCategory from 'pages/addCategory'
import AddEntry from 'pages/addEntry'
import Expenses from 'pages/expense'
import Income from 'pages/income'
import Entries from 'pages/entries'

import Logout from 'pages/logout'

//import react-bootstrap components
import {Container} from 'react-bootstrap'

//import react-router-dom components
import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'

//import CSS
import './App.css'

import {UserProvider} from './userContext'

function App() {



const [user,setUser] = useState({
  email: localStorage.getItem('email'),


  isAdmin: localStorage.getItem('isAdmin') === "true", 
  token: localStorage.getItem('token')

})

console.log(user)

const unsetUser = () => {


//clear method for localStorage allows us to clear out the items in the local storage.

  localStorage.clear()
}


  return ( 
    <> 
      <UserProvider value={{user,setUser,unsetUser}}>
        <Router> 
         <NavBar /> 
         <Container>
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/login" component={Login}/>
              <Route exact path="/register" component={Register}/>
              <Route exact path="/addCategory" component={AddCategory}/>
              <Route exact path="/addEntry" component={AddEntry}/>
              <Route exact path="/entries" component={Entries}/>
              <Route exact path="/expenses" component={Expenses}/>
              <Route exact path="/income" component={Income}/>
              <Route exact path="/logout" component={Logout}/>
             </Switch>   
          </Container>
        </Router> 
      </UserProvider>
    </>
  );
}

export default App;
