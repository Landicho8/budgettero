import React,{useContext} from 'react'


import {Navbar,Nav} from 'react-bootstrap'



import {Link,NavLink} from 'react-router-dom'

//import user context
import UserContext from 'userContext'

export default function NavBar(){



	const {user} = useContext(UserContext)
	console.log(user)

	return (

		<Navbar bg="light" expand="lg">
			<Navbar.Brand as={Link} to="/">Budgettero</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
	
				{
					user.email
					?
					<>
						<Nav.Link as={NavLink} to="/addCategory">Category</Nav.Link>
						<Nav.Link as={NavLink} to="/addEntry">Entry</Nav.Link>
						<Nav.Link as={NavLink} to="/entries">Entries</Nav.Link>
						<Nav.Link as={NavLink} to="/expenses">Expenses</Nav.Link>
						<Nav.Link as={NavLink} to="/income">Income</Nav.Link>
						<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
					</>
					:
					<>
						<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
						<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
					</>

				}
			</Navbar.Collapse>
		</Navbar>
)
}