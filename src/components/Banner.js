import React from 'react'

import {Jumbotron,Button,Row,Col} from 'react-bootstrap'


import {Link} from 'react-router-dom'


export default function Banner({dataProp}){



	const {title,description,label} = dataProp

	return (
		<>
			<Row>
				<Col>
				   <Jumbotron>
						<h1>{title}</h1>
						<p>{description}</p>
						<Link to="/login">{label}</Link>
					</Jumbotron>
				</Col>
			</Row>
		</>

	)

}
