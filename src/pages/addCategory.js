import React, {useState,useEffect} from 'react'

import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

import {Redirect} from 'react-router-dom'

export default function AddCategory() {


const [name, setName] = useState("");
const [type, setType] = useState("");
const [userId, setUserId] = useState("");
const [isActive, setIsActive] = useState("");


	useEffect(()=>{

		if(name !== "" && type !== ""){

			setIsActive(true)

		} else {

			setIsActive(false)


		}

	},[name,type,userId])

	
	function AddCategories(e){

		e.preventDefault()

		fetch('https://blooming-tundra-76126.herokuapp.com/api/categories/', {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json',

				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				type: type
			})
		})
		.then(res => res.json())
		.then(data => {

		console.log(data._id)

		if(data._id) {



		Swal.fire({

			icon: "success",
			title: "Successfully Added a Category.",
			text: "Congratulation you have added a Category."

		})
		} else {

		Swal.fire({

			icon: "error",
			title: "Category already exists",
			text: "Please create a new category"

		})

		}
	})
		//clear out the states to their initial values
		setName("")
		setType("")
		setUserId("")
	
	}

	return (
		<div>
			<h3 className="text-center">Category</h3>
			<Form onSubmit={e=> AddCategories(e)}>
			<Form.Group>
				<Form.Label>Name</Form.Label>
				<Form.Control type="text" placeholder="Enter Category Name" value={name} onChange={e=>{setName(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Type</Form.Label>
				<Form.Control as="select" placeholder="Select Type" value={type} onChange={e=>{setType(e.target.value)}} required>
				<option>Select Type</option>
				<option>Income</option>
				<option>Expense</option>
				</Form.Control>
			</Form.Group>
			{

				isActive
				?
				<Button variant="primary" type="submit">Submit</Button>
				:
				<Button variant="primary" type="submit" disabled>Add Category</Button>		

			}
			</Form>
		</div>
	)
}
