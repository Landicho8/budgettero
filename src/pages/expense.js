import React, {useState,useEffect,userContext} from 'react'

import {Form, Button} from 'react-bootstrap';

import Swal from 'sweetalert2';

import {Redirect} from 'react-router-dom'

import UserContext from 'userContext'

import{Table} from 'react-bootstrap'

export default function Expenses() {

const [category, setCategory] = useState("");
const [type, setType] = useState("");
const [amount, setAmount] = useState("");
const [userId, setUserId] = useState("");
const [isActive, setIsActive] = useState(false);
const [expense, setExpense] = useState([]);
const [expenseComponents, setExpenseComponents] = useState([]);
const [totalExpense,setTotalExpense] = useState([])

	useEffect(()=>{

			if(category !== "" && type !== "" && amount !== "") {

			setIsActive(true)
		} else {

			setIsActive(false);
		}

	},[category,type,amount]);

	useEffect(()=>{

			let token = localStorage.getItem('token');

			fetch('https://blooming-tundra-76126.herokuapp.com/api/entries/expenses', {

			headers: {					
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
			})
			.then(res => res.json())
			.then(data => {

			console.log(data)

		setExpenseComponents(data.map(entry => {
				return  (
					<tr>
						<td>{entry.category}</td>
						<td>{entry.type}</td>
						<td>{entry.amount}</td>
					</tr>

				)
			}))
					let totalExpense = data.map(element => {
				return element.amount
			})

			if (totalExpense.length >= 1) {
				console.log(totalExpense)
				let total = totalExpense.reduce((x,y) => x+y)
				console.log(total)
				setTotalExpense(total)
			}

		})
		},[])
	
			return(
			<>
				<h1 className="text-center mb-5">Expenses</h1>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Category</th>
							<th>Type</th>
							<th>Amount</th>			
						</tr>
					</thead>
					<tbody>
						{expenseComponents}
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2">Total Expense</td>			
							<td>{totalExpense}</td>
						</tr>
					</tfoot>
				</Table>
			</>
			);
		}