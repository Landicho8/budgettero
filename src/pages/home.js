import {useContext} from 'react'


import Banner from 'components/Banner'
import Highlights from 'components/Highlights'


import UserContext from 'userContext'

export default function Home(){


let myBudgettero = {
      title: "Budgettero",
      description: "Budgettero",
      label: "View Budget"
      
  }

	return (
		<>
        	<Banner dataProp={myBudgettero} />
       		 <Highlights />
        </>

		)
}