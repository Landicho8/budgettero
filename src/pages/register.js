import React,{useState,useEffect,useContext} from 'react'
import {Form, Button} from 'react-bootstrap'

import UserContext from 'userContext'

import {Redirect} from 'react-router-dom'

import Swal from 'sweetalert2'

export default function Register() {


const [firstName,setFirstName] = useState("")
const [lastName,setLastName] = useState("")
const [mobileNo,setMobileNo] = useState("")
const [email,setEmail] = useState("")
const [password,setPassword] = useState("")
const [confirmPassword,setConfirmPassword] = useState("")


const [isActive,setIsActive] = useState(true)

const [willRedirect,setWillRedirect] = useState(false)

const{user,setUser} = useContext(UserContext)

console.log(user)

	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") &&(mobileNo.length === 11) && (password === confirmPassword)){

			setIsActive(true)

		} else {

			setIsActive(false)

			//console. log("Check your input.")
		}

	},[firstName,lastName,mobileNo,password,confirmPassword])

	//submit function registerUser
	function registerUser(e){

		e.preventDefault()

		fetch('https://blooming-tundra-76126.herokuapp.com/api/users/', {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				email: email, 
				password: password,
				confirmPassword: confirmPassword
			})
		})
		.then(res => res.json())
		.then(data => {

			// console.log(data)

		Swal.fire({

			icon: "success",
			title: "Successful Registration.",
			text: "Congratulation you are now registered."
		})

		setWillRedirect(true)
	})
	
		setFirstName("")
		setLastName("")
		setEmail("")
		setMobileNo("")
		setPassword("")
		setConfirmPassword("")
	}

	return (
		user.email
	   	?
	    < Redirect to="/entries" />
	    :
		willRedirect
		?
		<Redirect to="/login"/>
		:
		<div>
			<h3 className="text-center">Register</h3>
			<Form onSubmit={e=> registerUser(e)}>
			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e=>{setFirstName(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e=>{setLastName(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email</Form.Label>
				<Form.Control type="text" placeholder="Enter Email" value={email} onChange={e=>{setEmail(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>MobileNo</Form.Label>
				<Form.Control type="text" placeholder="Enter 11-digit Mobile Number" value={mobileNo} onChange={e=>{setMobileNo(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control type="text" placeholder="Enter Password" value={password}onChange={e=>{setPassword(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>confirmPassword</Form.Label>
				<Form.Control type="text" placeholder="confirm Password" value={confirmPassword}onChange={e=>{setConfirmPassword(e.target.value)}}required/>
			</Form.Group>
			{

				isActive
				?
				<Button variant="primary" type="submit">Submit</Button>
				:
				<Button variant="primary" type="submit" disabled>Register</Button>		

			}
			</Form>
		</div>
	)
}
