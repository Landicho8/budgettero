import React,{useState,useEffect,useContext} from 'react'
import {Form, Button} from 'react-bootstrap'


import Swal from 'sweetalert2'


import {Redirect} from 'react-router-dom'


import UserContext from 'userContext'



export default function Login() {


const{user,setUser} = useContext(UserContext)
console.log(user)

const [email,setEmail] = useState("")
const [password,setPassword] = useState("")


const [isActive,setIsActive] = useState(true)

const [willRedirect,setWillRedirect] = useState(false)

/*
	console.log(email)
	console.log(password)

*/


	useEffect(()=>{

		if(email !== "" && password !== "") 
			{

			setIsActive(true)

		} else {

			setIsActive(false);

		
		}

	},[email,password])




		function loginUser(e){

		e.preventDefault()

		fetch('https://blooming-tundra-76126.herokuapp.com/api/users/login',{

			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data) 
			if(data.accessToken) {

			localStorage.setItem('token',data.accessToken)

			Swal.fire({

				icon: "success",
				title: "Logged In Successfully.",
				text: "Thank you for logging in Budget Tracker"

			})
			//get user's details from our token
			fetch('https://blooming-tundra-76126.herokuapp.com/api/users',{

				headers: {
					Authorization: `Bearer ${data.accessToken}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				//localStorage allows us to save data within our browser as strings.
				//.setItem() is used to store in the localstorage as strings.
				//.setItem('key',<value>)
				//Store the isAdmin property in our localStorage
				localStorage.setItem('email',data.email)
				localStorage.setItem('isAdmin',data.isAdmin)

				setWillRedirect(true)

				setUser({
					email: data.email,
					isAdmin: data.isAdmin
				})
			})

		} else {

			Swal.fire({
				icon: "error",
				title: "Login failed."
			})
		}
			
		})


		setEmail()
		setPassword("")
		}

	

	return (
	user.email
   	?
    < Redirect to="/entries" />
    :
		willRedirect
		?
		<Redirect to="/entries"/>
		:
		<div>
			<h3 className="text-center">Login</h3>
			<Form onSubmit={e=> loginUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email</Form.Label>
				<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e=>{setEmail(e.target.value)}} required/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>		
			</Form.Group>
			<Form.Group controlId="userPassword">
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder="Enter Password" value={password}onChange={e=>{setPassword(e.target.value)}} required/>
			</Form.Group>
			{

				isActive
				?
				<Button variant="primary" type="submit">Submit</Button>
				:
				<Button variant="primary" type="submit" disabled>Login</Button>

			}
			</Form>
		</div>
	)

}
