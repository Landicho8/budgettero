import React, {useState,useEffect,userContext} from 'react'

import {Form, Button} from 'react-bootstrap';

import Swal from 'sweetalert2';

import {Redirect} from 'react-router-dom'

import UserContext from 'userContext'

import{Table} from 'react-bootstrap'

export default function Expenses() {

const [category, setCategory] = useState("");
const [type, setType] = useState("");
const [amount, setAmount] = useState("");
const [userId, setUserId] = useState("");
const [isActive, setIsActive] = useState(false);
const [expense, setExpense] = useState([]);
const [expenseIncomeComponents, setExpenseIncomeComponents] = useState([]);

	useEffect(()=>{

			if(category !== "" && type !== "" && amount !== "") {

			setIsActive(true)
		} else {

			setIsActive(false);
		}

	},[category,type,amount]);

	useEffect(()=>{

			let token = localStorage.getItem('token');

			fetch('https://blooming-tundra-76126.herokuapp.com/api/entries/', {

			headers: {					
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
			})
			.then(res => res.json())
			.then(data => {

			console.log(data)

		setExpenseIncomeComponents(data.map(entry => {
				return  (
					<tr>
						<td>{entry.category}</td>
						<td>{entry.type}</td>
						<td>{entry.amount}</td>
					</tr>
				)
			}))
		})
		},[])
	
			return(
			<>
				<h1 className="text-center mb-5">Entries</h1>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Category</th>
							<th>Type</th>
							<th>Amount</th>			
						</tr>
					</thead>
					<tbody>
						{expenseIncomeComponents}
					</tbody>
				</Table>
			</>
			);
		}