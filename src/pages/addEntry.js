import React, {useState,useEffect,userContext} from 'react'

import {Form, Button} from 'react-bootstrap';

import Swal from 'sweetalert2';

import {Redirect} from 'react-router-dom'

import UserContext from 'userContext'

import{Table} from 'react-bootstrap'

export default function AddEntry() {

const [category, setCategory] = useState("");
const [type, setType] = useState("");
const [amount, setAmount] = useState("");
const [userId, setUserId] = useState("");
const [isActive, setIsActive] = useState(false);
const [expense, setExpense] = useState([]);
const [income, setIncome] = useState([]);
const [allCategory, setAllCategory] = useState("");

	useEffect(()=>{

			if(category !== "" && type !== "" && amount !== "") {

			setIsActive(true)

		} else {

			setIsActive(false);

		}

	},[category,type,amount]);


	useEffect(()=>{

			let token = localStorage.getItem('token');

			fetch('https://blooming-tundra-76126.herokuapp.com/api/categories/', {

			headers: {					
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
			})
			.then(res => res.json())
			.then(data => {
			console.log(data)
			
		const tempExpense = data.filter((category) => category.type === "Expense");
		const tempIncome = data.filter((category) => category.type === "Income");
				console.log(tempExpense)
				console.log(tempIncome)

		setExpense(tempExpense.map(category => {
			console.log(category.name)

			return(
				<option>{category.name}</option>
				)

		}))

		setIncome(tempIncome.map(category => {
				return (
					<option>{category.name}</option>)
		}))
			})

		},[type])


		console.log("hello")
		console.log(type)

			
	function AddEntries(e){

		e.preventDefault()


		fetch('https://blooming-tundra-76126.herokuapp.com/api/entries/', {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json',

				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				category: category,
				type: type,
				amount: amount
			})
		})
		.then(res => res.json())
		.then(data => {

		console.log(data)

		Swal.fire({

			icon: "success",
			title: "Successfully Added an Entry.",
			text: "Congratulation you have added an Entry."
		})

	})
		//clear out the states to their initial values
		setCategory("")
		setType("")
		setAmount("")
	
	}

	console.log(category)

	return (
		<div>
			<h3 className="text-center">Entry</h3>
			<Form onSubmit={e=> AddEntries(e)}>
			<Form.Group>
				<Form.Label>Type</Form.Label>
				<Form.Control as="select" placeholder="Select Type" value={type} onChange={e=>{setType(e.target.value)}} required>
					<option value="">Select Type</option>
					<option>Income</option>
					<option>Expense</option>
				</Form.Control>
			</Form.Group>
			<Form.Group>
				<Form.Label>Category</Form.Label>
				<Form.Control as="select" placeholder="Enter Category" value={category} onChange={e=>{setCategory(e.target.value)}} required>
				<option value="">Select Category</option>
				{type === "" ? null : type === "Expense" ? expense : income}
				</Form.Control>
			</Form.Group>
			<Form.Group>
				<Form.Label>Amount</Form.Label>
				<Form.Control type="number" placeholder="Enter Amount" value={amount} onChange={e=>{setAmount(e.target.value)}} required/>
			</Form.Group>
			{

				isActive
				?
				<Button variant="primary" type="submit">Submit</Button>
				:
				<Button variant="primary" type="submit" disabled>Add Entry</Button>		

			}
			</Form>
		</div>
	)
}
